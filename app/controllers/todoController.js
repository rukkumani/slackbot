const express = require("express"),
  router = express.Router(),
  mongoose = require("mongoose"),
  todoModel = mongoose.model("todoModel");

module.exports = function(app) {
  app.use("/", router);
};

router.post("/addtodo", async (req, res) => {
  if (req.body && Object.keys(req.body).length && req.body.text) {
    const { channel_id, team_id, text: task_name } = req.body;
    const newTodoModel = await new todoModel({
      channel_id,
      team_id,
      task_name,
      status: true
    });
    newTodoModel.save((err, result) => {
      if (err) {
        console.log(err);
        res.send({
          response_type: "in_channel",
          text: "Sorry Unable to process"
        });
      } else {
        res.send({
          response_type: "in_channel",
          text: `Added TODO for ${req.body.text}`
        });
      }
    });
  } else {
    res.send({
      response_type: "in_channel",
      text: `Please provide a Task Name`
    });
  }
});

router.post("/marktodo", async (req, res) => {
  if (req.body && Object.keys(req.body).length && req.body.text) {
    const { channel_id, team_id, text: task_name } = req.body;
    await todoModel
      .updateOne(
        {
          channel_id,
          team_id,
          task_name,
          status: true
        },
        { $set: { status: false } },
        { upsert: false, multi: false }
      )
      .then(
        value => {
          if (value && Object.keys(value).length && value.nModified == 1) {
            res.send({
              response_type: "in_channel",
              text: `Removed TODO for ${req.body.text}`
            });
          } else {
            res.send({
              response_type: "in_channel",
              text: `cant find out ${req.body.text}`
            });
          }
        },
        reject => {
          res.send({
            response_type: "in_channel",
            text: `Sorry Unable to process`
          });
        }
      )
      .catch(err => {
        res.send({
          response_type: "in_channel",
          text: `Sorry Unable to process`
        });
      });
  } else {
    res.send({
      response_type: "in_channel",
      text: `please provide Task`
    });
  }
});

router.post("/listtodos", async (req, res) => {
  console.log(req.body);
  if (req.body && Object.keys(req.body).length && req.body.channel_id) {
    const { channel_id } = req.body;
    todoModel
      .find({ channel_id, status: true })
      .then(value => {
        if (value && value.length) {
          const listtodo = value.map(task => task.task_name + "\n");

          res.send({
            response_type: "in_channel",
            text: `${listtodo}`
          });
        } else
          res.send({
            response_type: "in_channel",
            text: `No work to do`
          });
      })
      .catch(err => {
        res.send({
          response_type: "in_channel",
          text: `Sorry Unable to process`
        });
      });
  }
});
