const mongoose = require("mongoose"),
  schema = mongoose.Schema,
  todoSchema = new schema({
    channel_id: { type: String, required: true },
    team_id: { type: String, required: true },
    task_name: { type: String, required: true },
    status: { type: Boolean, required: true, default: true }
  });

mongoose.model("todoModel", todoSchema);
