const path = require("path"),
  rootPath = path.normalize(__dirname + "/.."),
  chalk = require("chalk"),
  env = process.env.NODE_ENV || "production",
  dbHost = process.env.DB_HOST_NAME,
  port = process.env.PORT;

var config = {
  production: {
    root: rootPath,
    port: port || 4001,
    db:
      "mongodb+srv://rukkumani:rukkumani@cluster0-yjs1z.mongodb.net/login?retryWrites=true"
  }
};
module.exports = config[env];
logConfiguration();
function logConfiguration() {
  console.log(chalk.styles.green.open + chalk.styles.green.close);
  console.log(
    "\n\n ---------------------Configuration in Use --------------------------"
  );
  console.log(config[env]);
}
